from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from api.views import SeekerViewSet, CityViewSet, CountryViewSet, StateViewSet, UniversityViewSet, MajorViewSet

router = routers.DefaultRouter()
router.register(r'seekers', SeekerViewSet)
router.register(r'cities', CityViewSet)
router.register(r'countries', CountryViewSet)
router.register(r'states', StateViewSet)
router.register(r'universities', UniversityViewSet)
router.register(r'majors', MajorViewSet)


urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls')),
    path('admin/', admin.site.urls),
]
