from django.contrib import admin
from .models import Seeker, University, Major

# Register your models here.
admin.site.register(Seeker)
admin.site.register(University)
admin.site.register(Major)