from api.models import Seeker, University, Major
from cities_light.models import City, Region, Country
from .serializers import SeekerSerializer, CitySerializer, CountrySerializer, RegionSerializer, \
    UniversitySerializer, MajorSerializer
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response


class SeekerViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows seekers to be viewed or edited.
    """
    queryset = Seeker.objects.all()
    serializer_class = SeekerSerializer

    @action(detail=False, methods=['post'])
    def process_form(self, request, pk=None):
        seeker = self.get_object()
        serializer = SeekerSerializer(data=request.data)
        if serializer.is_valid():
            seeker.first_name = "Bob Dylan"
            seeker.save()
            return Response({'status': 'data saved'})
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)


class CityViewSet(viewsets.ModelViewSet):
    queryset = City.objects.all()
    serializer_class = CitySerializer
    filterset_fields = ['region__name', 'country','country__name']


class StateViewSet(viewsets.ModelViewSet):
    queryset = Region.objects.all()
    serializer_class = RegionSerializer
    filterset_fields = ['geoname_code', 'country__name']


class CountryViewSet(viewsets.ModelViewSet):
    queryset = Country.objects.all()
    serializer_class = CountrySerializer
    filterset_fields = ['name', 'id']


class UniversityViewSet(viewsets.ModelViewSet):
    queryset = University.objects.all()
    serializer_class = UniversitySerializer
    filterset_fields = ['name']


class MajorViewSet(viewsets.ModelViewSet):
    queryset = Major.objects.all()
    serializer_class = MajorSerializer
    filterset_fields = ['name']
