from django.db import models


class Seeker(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    address_line_1 = models.CharField(max_length=150)
    address_line_2 = models.CharField(max_length=150, null=True, blank=True)
    city = models.CharField(max_length=100)
    state = models.CharField(max_length=100)
    postal_code = models.CharField(max_length=10)
    country = models.CharField(max_length=60)

    # TODO should we add a successful application field to know whether to use it in ML model

    def __str__(self):
        return self.first_name + ' ' + self.last_name

    # TODO add other fields


class University(models.Model):
    name = models.CharField(max_length=150, null=True)
    city = models.CharField(max_length=100, null=True)
    state = models.CharField(max_length=2, null=True)

    list_display = ('name','city','state')

    def __str__(self):
        return self.name


class Major(models.Model):
    name = models.CharField(max_length=150, null=True)
    category = models.CharField(max_length=200, null=True)

    def __str__(self):
        return self.name
